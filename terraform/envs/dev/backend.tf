terraform {
  backend "s3" {
    bucket = "interview-final-dev-bucket-ant-tech"
    key    = "terraform/dev/"
    region = "us-east-1"
  }
}